---
title: Adding a new gitlab-runner configuration
linkTitle: New gitlab-runner
description: >
    How to create a new gitlab-runner configuration and attach it to
    the correct projects
---

## Problem

A new gitlab-runner needs to be attached to a project.

## Steps

1. Checkout `deployment-all` and export the encryption password:

   ```bash
   export ENVPASSWORD=....
   ```

1. Verify that the checkout works correctly including access to AWS and Beaker:

   ```bash
   aws_saml_login.sh --role poweruser
   gitlab-runner-config/deploy.sh configurations diff
   ```

1. Add the new gitlab-runner to

   - Beaker machine:  `ansible/inventory/beaker.yml`, `all.hosts.children.gitlab_runner.hosts`
   - Other machine: `gitlab-runner-config/deploy.sh`, `RUNNER_HOSTS`
   - Kubernetes: `gitlab-runner-config/deploy.sh`, `CONTEXTS`

   Also add it to `gitlab-runner-config/runners.yml.j2` in `runner_deployments`.

   Depending on the type, setup and execute the necessary playbooks the
   configure the machine.

1. If a new configuration template is needed, add it to
   `gitlab-runner-config/runners.yml.j2` in `runner_templates`.

1. Instantiate the template in the correct runner deployment via the
   `runner_configurations` variable. If a new configuration is created, make
   sure it is attached to the correct GitLab projects via
   `runner_registrations`.

1. Run `gitlab-runner-config/deploy.sh registrations diff` and check that the
   new runner configurations are reported and do not have a token assigned.

1. Run `gitlab-runner-config/deploy.sh registrations apply --create-missing`
   and add the new tokens to the secrets via `./edit_secret.sh secrets.yml
   GITLAB_RUNNER_TOKENS`.

1. Deploy the new configurations via `gitlab-runner-config/deploy.sh
   configurations apply`.
