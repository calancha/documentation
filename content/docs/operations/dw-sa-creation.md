---
title: Creation of service accounts
description: >
    How to create service account for DataWarehouse
---

## Problem

You need a service account for DataWarehouse.

## Steps

1. Login to [DataWarehouse] and navigate to [admin panel of DataWarehouse]

2. Click "+ Add" on Users model and create a new user by clicking "Save and continue editing".

3. Scroll down and add any value to last_login, this prevents the account getting deleted.

4. Add any needed groups.

5. (optional) Add api token (under the Tokens model) to the new service account,
   and if needed add it to deployment-all.

### Notes

- Commonly service accounts username start with `sa-`.
- It is best to use a random password generator for the service account like [Diceware] or [BitWarden],
  however if the service account doesn't need password after creation of the service account you can
  unset the password by opening [shell] and running this (example using the username 'sa-new-user'):

  ```Python
  sa_user = User.objects.get(username='sa-new-user')
  sa_user.set_unusable_password()
  sa_user.save()
  ```

[DataWarehouse]: https://datawarehouse.cki-project.org/
[admin panel of DataWarehouse]: https://datawarehouse.cki-project.org/admin/
[Diceware]: https://diceware.dmuth.org/
[BitWarden]: https://vault.bitwarden.com/#/tools/generator
[shell]: https://cki-project.org/docs/datawarehouse/handbook/
