---
title: "CKI-010: Tracking and planning"
linkTitle: CKI-010
description: >
    How to track both planned and unplanned work, and create a reliable
    milestone planning
layout: rfc
author: Michael Hofmann
mr: 275
---

## Abstract

This document describes an approach to handle planned and unplanned work based
on GitLab issues.

## Motivation

The CKI team has started to use weights on issues, but the precise approach for
tracking planned and unplanned work has not been written down and discussed
properly.

## Approach

[GitLab issues] are used for tracking work items. Ideally, each issue should
represent the smallest deliverable change.

### Issue life cycle

Newly created issues are automatically tagged with the `needs-triage` label.
During the daily stand-up meetings, these issues are shortly presented by
whoever created the issue. This is done even if an issue is already closed.

It is easily possible to get a list of those issues. Possible implementations
include

- on the `standup` IRC bot command, print a link to the GitLab issue list
  filtered by the label `needs-triage` label
- on the `standup` IRC bot command, print a list of the issues
- implement a dedicated `list-triage` IRC bot command

The `needs-triage` label can be removed from all issues via the `clear-triage`
IRC bot command after the stand-up meeting.

Closed issues are automatically tagged with a date-based `sprint` label such as
`sprint::2022-week-32`. If no milestone was associated with an issue, an extra
milestone-based `unplanned` label such as `unplanned::Plumbers 2022` is
added.

### Planned work

Work across a calendar year is divided into three intervals ending at the
following conferences:

- [DevConf.CZ] in the winter
- [Red Hat Summit] in the spring
- [Linux Plumbers] in the fall

To track planned work for these [milestones], GitLab issues are tagged with the
milestone, e.g. with the `/milestone` quick command or the `m` keyboard
shortcut in the issue view.

For issues to be included in a milestone, they must have a `weight` assigned
according to the following table:

| Weight | Time     | Complexity                                                                                |
|--------|----------|-------------------------------------------------------------------------------------------|
| `2`    | 1-2 days | Simple: minimal amount of work, acceptance criteria clear and manageable.                 |
| `5`    | 3-5 days | Complicated: task has some difficult aspects, but acceptance criteria are still feasible. |
| `8`    | Unknown  | Too big: Task is too big, amount of work is not predictable, break into smaller tasks.    |

Only issues with a weight below or equal to `5` can be included in a milestone.
Issues with a higher weight must be split up, e.g. by converting them into an
epic and creating issues for the individual work items. Where needed, this
split has to be completed before the milestone planning meeting.

The total weight of all issues in a new milestone is based on the weight of
issues completed in previous milestones (velocity).

After planning for a milestone is finalized,

- weights of associated issues must not be changed
- no new issues may be added, unless issues with an equal weight are removed
  from the milestone

### Unplanned work

Unplanned work is any work not included in the planning, e.g. urgent feature
requests, bug fixes, security-related infrastructure work, maintenance and
incidents.

To make it easy to create issues for unplanned work, the IRC bot can create
issues via the `new-issue`, `new-incident` and `issue-from-mr` commands.

As opposed to planned work, no milestone should be assigned to the associated
GitLab issues. For incidents, the issues should be tagged with the `incident`
label, e.g. via the `/label` quick command or the `l` keyboard shortcut in the
issues view.

When closed, unweighted issues will be automatically tagged with a weight of
`2`.

## Benefits

For each work item, a GitLab issue should be created. This

- creates visibility and enables reporting
- allows to attach merge requests to it, which provides a way to later track
  down all the work required for a certain work item

The `needs-triage` label simplifies the review of newly created work items
during the daily stand-up meetings. Daily review of these issues ensures that

- everybody is aware of the work items
- knowledge transfer is facilitated by allowing everybody to raise their hand
  to work on an item, and request help and mentorship directly if they feel
  they do not have enough experience to resolve the issues on their own

The `sprint` labels simplify the review of finished work during the backlog
review meetings. They provide an indication when issues were closed and
therefore whether they should be discussed in the backlog review meeting.

The `unplanned` labels simplify the review of finished unplanned work during
the milestone review meetings. They provide an indication of the amount of
unplanned work that occurred during a milestone. A label is used as opposed to
a milestone to emphasize the unplanned aspect of the work, while still
allowing easy alignment with the corresponding milestone.

Assigning `weights` for all issues included in the planning allows to determine
the velocity of development, which

- enables a more precise planning
- creates flexibility as it allows to replace planned issues by new issues as
  long as total weight of the milestone is maintained

## Drawbacks

The process for following up on weight 8 issues getting broken down is
currently not specified. For large tasks, this breakdown can take a long time.

The approach as described above might create a bunch of busywork and stress,
only serving to slow the team down and give just a semblance of control and
predictability.

[GitLab issues]: https://gitlab.com/groups/cki-project/-/issues

[DevConf.CZ]: https://www.devconf.info/cz/
[Red Hat Summit]: https://www.redhat.com/en/summit
[Linux Plumbers]: https://lpc.events/

[milestones]: https://gitlab.com/groups/cki-project/-/milestones

<!-- vi: set spell spelllang=en: -->
